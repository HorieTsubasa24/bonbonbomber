﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZunkoDP : MonoBehaviour
{
    public SkinnedMeshRenderer mesh;
    public SkinnedMeshRenderer meshI;
    // Update is called once per frame
    void Update()
    {
        mesh.SetBlendShapeWeight(0, 100f);
        meshI.SetBlendShapeWeight(0, 50f);
    }
}
