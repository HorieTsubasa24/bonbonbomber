﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSSoft
{
    public class move : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody rigid;
        [SerializeField]
        private float degree = 0;
        public float Degree { get { return degree;  } }
        [SerializeField]
        private Vector3 diff = new Vector3(8.03f, 10.92f, 36.11f);

        // Update is called once per frame
        void Update()
        {
            float rad = degree * Mathf.Deg2Rad; //角度をラジアン角に変換
            Vector3 degVector = new Vector3(Mathf.Cos(rad), 0, Mathf.Sin(rad));

            var x = Input.GetAxis("Horizontal");
            if (x > 0.5f)
                degree += 8;
            else if (x < -0.5f)
                degree -= 8;
            degree = degree % 360.0f;

            var f = Input.GetAxis("Vertical");
            if (f > 0.5f)
                rigid.AddForce(degVector * 5);
            else if (f < -0.5f)
                rigid.AddForce(degVector * -5);

            transform.position = rigid.transform.position + diff;
            transform.rotation = Quaternion.Euler(new Vector3(0, degree, 0));
        }
    }
}