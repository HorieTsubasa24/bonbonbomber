﻿using DG.Tweening;
using System;
using System.Collections;
using System.Threading.Tasks;
using UniRx;
using UnityEngine;
using UnityEngine.U2D;

namespace TSSoft
{
    /// <summary>
    /// 画面のフェードイン・アウトのコントローラー
    /// </summary>
    public class FadeController : SingletonMonoBehaviour<FadeController>
    {
        private const string BUNDLE_NAME = "{0}/{1}/tex_0";
        private const string ATLAS_NAME = "FadeAtlas";
        private readonly string[] SPRITE_NAMES = { "05", "06", "08", "09", "13", "14", "15", "17",
            "18", "19", "21", "22", "29", "30", "31", "32", "33", "34", "38", "39", "41", "46",
            "51", "52", "53", "54", "79", "80", "81", "95"};
#if UNITY_EDITOR
        private const string RESOURCES_ATLAS_NAME = "ruleA/FadeAtlas";
#endif

        public bool isResoures = false; // Resourcesから読み込むときはこちら

        public FadeImage fadeImage;
        public Sprite[] sprites;

        private System.Random rand = new System.Random();

        private Tween tween;

        /// <summary>
        /// 初期化
        /// </summary>
        public async Task<bool> Init()
        {
            this.fadeImage.InitMaterial();
            this.fadeImage.Range = 1.0f;
            await this.StartCoroutine(this.Initing());
            return true;
        }

        /// <summary>
        /// 初期化
        /// </summary>
        private IEnumerator Initing()
        {
            this.sprites = new Sprite[SPRITE_NAMES.Length];
            // TODO:nowloading
            if (!this.isResoures)
            {
                string path = string.Format(BUNDLE_NAME, Application.streamingAssetsPath, Platform.Instance.GetPlatform());
                Debug.Log(path);
                var op1 = AssetBundle.LoadFromFileAsync(path);
                yield return op1;
                var texAssets = op1.assetBundle;
                var atlas = texAssets.LoadAsset<SpriteAtlas>(ATLAS_NAME);
                atlas.GetSprites(sprites);
            }
#if UNITY_EDITOR
            else
            {
                var atlas = Resources.Load(RESOURCES_ATLAS_NAME, typeof(SpriteAtlas)) as SpriteAtlas;
                atlas.GetSprites(sprites);
            }
#endif
            // TODO:nowloading
        }

        /// <summary>
        /// スプライトをランダムで取得
        /// </summary>
        /// <returns></returns>
        private Texture GetTextureRandom()
        {
            Sprite sp = sprites[this.rand.Next(sprites.Length - 1)];
            Rect rect = sp.textureRect;
            Color[] pixels = sp.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
            Texture2D clipTex = new Texture2D((int)rect.width, (int)rect.height);
            clipTex.SetPixels(pixels);
            clipTex.Apply();
            return clipTex;
        }

        /// <summary>
        /// フェードイン処理
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="delay"></param>
        /// <param name="callback"></param>
        public void Fadein(float duration, float delay = 0.0f, Action callback = null)
        {
            this.fadeImage.UpdateMaskTexture(GetTextureRandom());

            if (this.tween != null && this.tween.IsPlaying())
            {
                this.tween.Kill();
                this.tween = null;
            }

            this.tween = DOVirtual.DelayedCall(delay, () =>
            {
                this.fadeImage.Range = 1.0f;
                DOTween.To(() =>
                    this.fadeImage.Range,
                    num => this.fadeImage.Range = num,
                    0.0f,
                    duration)
                .OnComplete(() => callback?.Invoke());
            });
        }

        /// <summary>
        /// フェードアウト処理
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="delay"></param>
        /// <param name="callback"></param>
        public void Fadeout(float duration, float delay = 0.0f, Action callback = null)
        {
            this.fadeImage.UpdateMaskTexture(GetTextureRandom());

            if (this.tween != null && this.tween.IsPlaying())
            {
                this.tween.Kill();
                this.tween = null;
            }

            this.tween = DOVirtual.DelayedCall(delay, () =>
            {
                this.fadeImage.Range = 0.0f;
                DOTween.To(() =>
                    this.fadeImage.Range,
                    num => this.fadeImage.Range = num,
                    1.0f,
                    duration)
                .OnComplete(() => callback?.Invoke());
            });
        }
    }
}
