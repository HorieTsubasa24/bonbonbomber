﻿using UnityEngine;
using UnityEngine.UI;

namespace TSSoft {
    /// <summary>
    /// ロード中の表示
    /// </summary>
    public class NowLoading : SingletonMonoBehaviour<NowLoading>
    {
        public GameObject fiiiObject;
        public Image fiiiImage;

        /// <summary>
        /// 初期化
        /// </summary>
        public void Init()
        {
            this.SetFill(0);
            this.ToVisible(false);
        }

        /// <summary>
        /// 表示非表示
        /// </summary>
        /// <param name="toVisible"></param>
        public void ToVisible(bool toVisible)
        {
            this.fiiiObject.SetActive(toVisible);
        }

        /// <summary>
        /// ゲージを更新
        /// </summary>
        /// <param name="value"></param>
        public void SetFill(float value)
        {
            this.fiiiImage.fillAmount = value;
        }
    }
}
