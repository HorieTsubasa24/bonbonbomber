﻿using UnityEngine;
using UniRx;

namespace TSSoft
{
    /// <summary>
    /// 初期化用クラス
    /// </summary>
    public class BootLoader : SingletonMonoBehaviour<BootLoader>
    {
        public bool IsLoaded { get; private set; }

        protected override async void Start()
        {
            base.Start();
            if (instance != this)
            {
                return;
            }

            NowLoading.Instance.Init();
            NowLoading.Instance.ToVisible(true);
            if (!await FadeController.Instance.Init())
            {
                Debug.LogError(" FadeController.Instance.Init() 初期化が失敗しました");
            }
            NowLoading.Instance.SetFill(1.0f);
            NowLoading.Instance.ToVisible(false);
            SceneChanger.Instance.Init();
            IsLoaded = true;
        }
    }
}
