﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSSoft
{
    /// <summary>
    /// 現在のゲームの状態
    /// </summary>
    public enum GameState
    {
        NONE = -1,
        INTORO = 0,
        GAME = 1,
        END_GAME = 2,
    }
}