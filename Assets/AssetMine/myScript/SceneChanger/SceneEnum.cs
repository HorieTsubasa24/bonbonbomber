﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSSoft
{
    /// <summary>
    /// シーンを表す識別子
    /// </summary>
    public enum SceneEnum
    {
        NONE = -1, 
        TITLE = 0,
        OPTION = 1,
        HOWTOPLAY = 2,
        STORY = 3,
        GAME = 4,
    }
}
