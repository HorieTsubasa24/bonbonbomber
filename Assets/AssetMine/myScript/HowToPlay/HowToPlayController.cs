﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace TSSoft
{
    /// <summary>
    /// ゲームメニューのコントローラー
    /// </summary>
    public class HowToPlayController : MonoBehaviour
    {
        public float fadeDuration = 1.0f;
        public RectTransform cursor;
        public RectTransform[] cursorCuide;
        private bool isCanControll = false;
        private int cursorIndex = 0;

        private float tim;

        // Start is called before the first frame update
        void Start()
        {
            FadeController.Instance.Fadein(fadeDuration, 0, () => isCanControll = true);
            tim = Time.time + fadeDuration;
        }

        // Update is called once per frame
        void Update()
        {
            if (!isCanControll || tim > Time.time)
                return;

            float y = PlayerController.Instance.joy_Axis_buf.y;
            if (y > 0)
            {
                cursorIndex = Mathf.Clamp(cursorIndex - 1, 0, cursorCuide.Length - 1);
                ChangeCursor();
            }
            else if (y < 0)
            {
                cursorIndex = Mathf.Clamp(cursorIndex + 1, 0, cursorCuide.Length - 1);
                ChangeCursor();
            }

            if (PlayerController.Instance.isFire1)
            {
                if (cursorIndex == 0)
                    ToGameStart();
                else if (cursorIndex == 1)
                    ToHowToPlay();
                else
                    ToHiScore();
            }
        }

        /// <summary>
        /// ゲームスタートがクリックされたときの処理
        /// </summary>
        public void GameStartOnClick()
        {
            ToGameStart();
        }

        /// <summary>
        /// あそびかたがクリックされたときの処理
        /// </summary>
        public void HowToPlayOnClick()
        {
            ToHowToPlay();
        }

        /// <summary>
        /// ハイスコアがクリックされたときの処理
        /// </summary>
        public void HiScoreOnClick()
        {
            ToHiScore();
        }
        
        /// <summary>
        /// ゲームスタートの処理
        /// </summary>
        private void ToGameStart()
        {
            isCanControll = false;
            FadeController.Instance.Fadeout(fadeDuration, 0, () => SceneChanger.Instance.GotoScene(SceneEnum.STORY));
        }

        /// <summary>
        /// カーソルを変更する処理
        /// </summary>
        private void ChangeCursor()
        {
            // TODO:サウンド
            cursor.position = cursorCuide[cursorIndex].position;
        }

        /// <summary>
        /// あそびかたの処理
        /// </summary>
        private void ToHowToPlay()
        {

        }

        /// <summary>
        /// ハイスコアの処理
        /// </summary>
        private void ToHiScore()
        {

        }
    }
}