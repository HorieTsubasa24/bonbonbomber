﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TSSoft
{
    /// <summary>
    /// ストーリー語りのコントローラー
    /// </summary>
    public class StoryController : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            FadeController.Instance.Fadein(0.5f);
            DG.Tweening.DOVirtual.DelayedCall(2.0f, () => SceneChanger.Instance.GotoScene(SceneEnum.GAME));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}