﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSSoft
{
    public class cam : MonoBehaviour
    {
        private Transform myTra;
        [SerializeField]
        private move shp;
        [SerializeField]
        private Vector3 diff;
        // Start is called before the first frame update
        void Start()
        {
            myTra = shp.transform;
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = myTra.position + diff;
            transform.rotation = Quaternion.Euler(new Vector3(0, shp.Degree, 0));
        }
    }
}
